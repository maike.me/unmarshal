package unmarshal

import (
	"net/http/httptest"
	"strings"
	"testing"
)

func TestJSONFromHTTPRequestToInterface(t *testing.T) {
	var formData struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "http://example.com/foo", nil)
	if err := JSONFromHTTPRequestToInterface(w, r, &formData); err == nil {
		t.Errorf("JSONFromHTTPRequestToInterface() err = %v want 'unexpected end of JSON input'", err)
	}

	bodyReader := strings.NewReader(`{"username": "MyUser", "Password": "MyPassword"}`)
	r = httptest.NewRequest("GET", "http://example.com/foo", bodyReader)
	if err := JSONFromHTTPRequestToInterface(w, r, &formData); err != nil {
		t.Errorf("JSONFromHTTPRequestToInterface() err = %v want nil", err)
	}
	if formData.Username != "MyUser" || formData.Password != "MyPassword" {
		t.Errorf("JSONFromHTTPRequestToInterface() formData.Username = %v want MyUser", formData.Username)
	}
}
