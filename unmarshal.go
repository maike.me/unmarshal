package unmarshal

import (
	"encoding/json"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"time"
)

func JSONFromHTTPRequestToInterface(w http.ResponseWriter, r *http.Request, target interface{}) error {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		return err
	}
	if err = json.Unmarshal(body, &target); err != nil {
		return err
	}
	return nil
}

func XMLFromHTTPRequestToInterface(w http.ResponseWriter, r *http.Request, target interface{}) error {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		return err
	}
	if err = xml.Unmarshal(body, &target); err != nil {
		return err
	}
	return nil
}

func JSONFromURLToInterface(url string, target interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bodyBytes, &target); err != nil {
		return err
	}
	return nil
}

func JSONFromURLToInterfaceWithTimeout(url string, target interface{}, timeout time.Duration) error {
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bodyBytes, &target); err != nil {
		return err
	}
	return nil
}

func XMLFromURLToInterface(url string, target interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err = xml.Unmarshal(bodyBytes, &target); err != nil {
		return err
	}
	return nil
}

func XMLFromURLToInterfaceWithTimeout(url string, target interface{}, timeout time.Duration) error {
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	if err = xml.Unmarshal(bodyBytes, &target); err != nil {
		return err
	}
	return nil
}

func MapToInterface(m map[string]interface{}, target interface{}) error {
	mapBytes, err := json.Marshal(m)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(mapBytes, &target); err != nil {
		return err
	}
	return nil
}
